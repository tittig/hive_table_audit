#fonction pour auditer une database
def get_audit_tables(database):
    list_audit = []
    #get list table
    tables = spark.sql("show tables in " + database).select("tableName").rdd.map(lambda x : x.tableName).collect()
    
    for table in tables :
        list_audit.append(get_audit_table(database,table))
        
    return list_audit
    

#fonction pour récupérer les infos importante d'une table
def get_audit_table(database,table):
    import subprocess
    import re
    
    outtable = {}
    Partition = False
    
    #get info table
    describe_table = spark.sql("DESCRIBE FORMATTED "+database+"."+table).rdd.map(lambda x : [x.col_name,x.data_type]).collect()
    
    for el in describe_table :
        if 'Type' in el :
            Type = el[1]
        if 'Location' in el :
            Location = el[1]
        if '# Partition Information' in el :
            Partition = True
        if 'InputFormat' in el :
            InputFormat = el[1]
        if 'OutputFormat' in el :
            OutputFormat = el[1]
        if 'Table' in el :  
            TableName = el[1]
    
    outtable["Table"]=TableName
    print(TableName)
    outtable["Type"]=Type
    print(Type)
    
    if Type != "VIEW" :
        outtable["Location"]=Location
        outtable["InputFormat"]=InputFormat
        outtable["OutputFormat"]=OutputFormat
        outtable["Partitioned"]=Partition
        #get info files
        process = subprocess.Popen("hdfs dfs -ls -R "+ Location +" | sed 's/  */ /g' | cut -d\  -f 1,5 --output-delimiter=',' | grep ^- | cut -d, -f2", stdout=subprocess.PIPE, shell=True)
        stdout, stderr = process.communicate()
        liste = list(map(int, stdout.decode("utf-8").splitlines()))
        #1 048 576 = 1Mo
        nbFiles=len(liste)
        nbSmallFiles=sum(i < 104857600 for i in liste)

        outtable["nbFiles"]=nbFiles
        outtable["nbSmallFiles"]=nbSmallFiles

        process = subprocess.Popen("hdfs dfs -du -h -s " + Location + " | sed 's/  */ /g' | cut -d\  -f 1,2", stdout=subprocess.PIPE, shell=True)
        stdout, stderr = process.communicate()
        
        if len(stdout.decode("utf-8").splitlines())!= 0 :
            if "/" not in stdout.decode("utf-8").splitlines()[0] :
                outtable["filesSize"]=stdout.decode("utf-8").splitlines()[0]
            else :
                outtable["filesSize"]=re.sub("h.*$", "", stdout.decode("utf-8").splitlines()[0])

        
        
    else :
        outtable["Location"]="View"
        outtable["InputFormat"]="View"
        outtable["OutputFormat"]="View"
        outtable["Partitioned"]=False
        outtable["nbFiles"]=0
        outtable["nbSmallFiles"]=0
        outtable["filesSize"]="0"
    
    return outtable